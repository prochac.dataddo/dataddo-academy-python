size = 5
character = "*"

# print a square of given size made of given characters
# *****
# *****
# *****
# *****
# *****

# print a right triangle
# *
# **
# ***
# ****
# *****

# print a isosceles triangle
#     *
#    ***
#   *****
#  *******
# *********

# print a diamond
#    *
#   ***
#  *****
#   ***
#    *
