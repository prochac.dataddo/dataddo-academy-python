cars = [
    {
        # key  : value
        "plate": "ABC1",
        "color": "blue",
    },
    {"plate": "ABC2", "color": "red"},
    {"plate": "ABC3", "color": "green"},
    {"plate": "ABC4", "color": "blue"},
]

plate = "ABC1"

index_of_car = "nothing"
for i, car in enumerate(cars):
    if car["plate"] == plate:
        index_of_car = i

print("index of the car:", index_of_car)
