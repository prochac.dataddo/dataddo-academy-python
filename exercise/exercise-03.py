matrix = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
]

# Convert matrix to 2D array
# matrix_as_list should contain [1, 2, 3, 4, 5, 6, 7, 8, 9]
matrix_as_list = []

# Reverse matrix_as_list list
# reversed_list should contain [9, 8, 7, 6, 5, 4, 3, 2, 1]
reversed_list = []

# Create reversed matrix
# reversed_matrix should contain [[9, 8, 7], [6, 5, 4], [3, 2, 1]]
reversed_matrix = []

# test your implementation with bigger matrix
# The '0x' prefix means that the number is inserted as hexadecimal.
# '0b' is for binary => 0b10 => 2
# '0o' is for octal => 0o10 => 8
# '0x' is for hexadecimal => 0x10 => 16
bigger_matrix = [
    [0x0, 0x1, 0x2, 0x3],
    [0x4, 0x5, 0x6, 0x7],
    [0x8, 0x9, 0xA, 0xB],
    [0xC, 0xD, 0xE, 0xF],
]
