#!python3

# Get input from console, radius of some circle, and then calculate area of the circle from given radius.
print("### Area of a Circle calculator ###")
print("Insert a radius of the circle:")
raw_radius = input()
radius = float(raw_radius)

from math import pi

area = radius ** 2 * pi

print("The area is:", area)
print()

# get input from console and print a vehicle
# if the input is one wheel, print unicycle
# if the input is two wheel, print bicycle
# if the input is three wheel, print tricycle
# if the input is for wheel, print car

vehicles = [
    "unicycle",
    "bicycle",
    "tricycle",
    "car",
]

print("### Guess the vehicle ###")
print("How many wheels the vehicle has?")

raw_wheel_num = input()
wheel_num = int(raw_wheel_num)

if wheel_num < 1:
    print("not enough wheels")
elif wheel_num > 4:
    print("too much wheels")
else:
    vehicle = vehicles[wheel_num - 1]
    print("Your vehicle is", vehicle)

# get input from console, the birth year, and decide to which generation it
# belongs to.
# Because the definitions are overlapping, choose your own algorithm how to deal with it.
# https://upload.wikimedia.org/wikipedia/commons/3/3e/Generation_timeline.svg

print("### Generation sorter ###")
print("What is your birth year?")

raw_year = input()
year = int(raw_year)

generations = {
    "Lost Generation": (1883, 1900),
    "G.I. Generation": (1901, 1927),
    "Silent Generation": (1928, 1945),
    "Baby Boomers": (1946, 1964),
    "Generation X": (1865, 1880),
    "Generation Y": (1981, 1996),
    "Generation Z": (1997, 2012),
    "Generation Alpha": (2013, 2022),
}

gen = "unknown"
for name, boundaries in generations.items():
    start, end = boundaries
    if start <= year <= end:
        gen = name
        break

print("You are", gen)

# Make this task in steps:
#  - Make a dice roll => get random number 1-6.
#           If you don't know, ask Google how to
#           "Generate random numbers between specific range in Python".
#  - Repeat it 6-times and store the results.
#  - Check, if you have straight.
#          If you don't know how, ask me :)
#  - Repeat until you don't have one
#  - Count and print the number of tries.

print("### Dice Roll ###")
from random import randint

tries_count = 0
is_straight = False
while not is_straight:
    tries_count += 1
    roll_results = []
    for _ in range(6):
        roll_result = randint(1, 6)
        roll_results.append(roll_result)
    is_straight = set(roll_results) == {1, 2, 3, 4, 5, 6}

roll_results.sort()
print(roll_results, "on", tries_count, "th try")
