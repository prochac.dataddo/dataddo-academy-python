#!/usr/bin/env python3

import webbrowser
import urllib.parse
from io import StringIO

print("#" * 80)
print("##{:^76}##".format("Hello!"))
print("##{:^76}##".format("Wellcome in this friendly quiz."))
print("#" * 80)
print("##{:^76}##".format("Please, pick your answer from provided list by selecting its index."))
print("##{:^76}##".format("Don't forget, indexing starts from zero in Python!"))
print("#" * 80)
print()
print()


class Question:
    def __init__(self, question: tuple, options: list, answer_index: int):
        self.question = question
        self.options = options
        self.answer_index = answer_index

    def __str__(self):
        options = StringIO()
        options.write("[")
        print(*self.options, sep=", ", file=options, end='')
        options.write("]")
        return "> {}".format(self.question[0]) + "\n" + \
               ">\t{}".format(self.question[1]) + "\n" + \
               "Options: {}".format(options.getvalue()) + "\n"

    def is_correct(self, index: int) -> bool:
        try:
            return self.options[self.answer_index] == self.options[index]
        except IndexError:
            print("Invalid index. You failed this question.")
            return False

    def correct_answer(self):
        return self.options[self.answer_index]


questions = [
    Question(  # 1
        ("What is the type of", "False"),
        ['int', 'float', 'bool', 'tuple', 'list', 'dict', 'set', 'str'],
        0b10,
    ),
    Question(  # 2
        ("What is the type of", "42"),
        ['dict', 'int', 'float', 'str', 'bool', 'list', 'tuple', 'set'],
        0b01,
    ),
    Question(  # 3
        ("What is the type of", "4.2"),
        ['set', 'int', 'dict', 'bool', 'list', 'float', 'tuple', 'str'],
        0o10 - 3,
    ),
    Question(  # 4
        ("What is the type of", "\"4.2\""),
        ['int', 'list', 'dict', 'tuple', 'float', 'set', 'str', 'bool'],
        0xB - 0x0d,
    ),
    Question(  # 5
        ("What is the type of", "[4, 2]"),
        ['tuple', 'dict', 'set', 'int', 'list', 'str', 'float', 'bool'],
        0xE - 10,
    ),
    Question(  # 6
        ("What is the type of", "(4, 2)"),
        ['float', 'list', 'str', 'dict', 'set', 'int', 'bool', 'tuple'],
        0xA - 0xB,
    ),
    Question(  # 7
        ("What is the type of", "{4, 2}"),
        ['tuple', 'int', 'list', 'set', 'float', 'dict', 'bool', 'str'],
        0xD - 0xA,
    ),
    Question(  # 8
        ("What is the type of", "{4: 2}"),
        ['int', 'tuple', 'bool', 'str', 'float', 'set', 'list', 'dict'],
        0xA - 0xB,
    ),
    Question(  # 9
        ("What is the result of", "1 + 1"),
        ["1", "1.0", "2", "2.0", "11", "1.1"],
        0xD - 0xB,
    ),
    Question(  # 10
        ("What is the result of", "1.0 + 1"),
        ["1", "1.0", "2", "2.0", "11", "1.1"],
        0xD - 0xA,
    ),
    Question(  # 11
        ("What is the result of", "5 ** 3"),
        ["5", "15", "15.0", "50", "55", "125"],
        0xF - 0xA,
    ),
    Question(  # 12
        ("What is the result of", "32 % 6"),
        ["2", "5", "5.3", "5.33333333", "6"],
        0x0A % 0o12,
    ),
    Question(  # 13
        ("What is the result of", "32 // 6"),
        ["2", "5", "5.3", "5.33333333", "6"],
        0x0A - 0o11,
    ),
    Question(  # 14
        ("What is the index of element 42", "[2, 22, 42, 44]"),
        ["4", "3", "2", "1", "0"],
        0x0A - 0b1000,
    ),
    Question(  # 15
        ("What is the index of element 42", "[2, 22, 42, 44]"),
        ["0", "-1", "-2", "-3", "-4"],
        0x0B - 9,
    ),
    Question(  # 16
        ("Is following statement true?", "3 < 5"),
        ["False", "True"],
        int(not False),
    ),
    Question(  # 17
        ("Is following statement true?", "3 < 5 < 3"),
        ["False", "True"],
        int(not not False),
    ),
    Question(  # 18
        ("Is following statement true?", "3 <= 3 <= 3"),
        ["False", "True"],
        int(not not True),
    ),
    Question(  # 19
        ("Is following statement true?", "\"foo\" is [\"foo\"]"),
        ["False", "True"],
        int(not not False),
    ),
    Question(  # 20
        ("Is following statement true?", "\"foo\" in [\"foo\"]"),
        ["False", "True"],
        int(not not True),
    ),
    Question(  # 21
        ("What is the variable contains?", "x = \"Fokume\"[0::2]"),
        ["F", "Fok", "Fme", "Fkm", "F   me"],
        0b11,
    ),
]


def get_index_input() -> int:
    while True:
        raw_input = input("Insert the index of your answer: ")
        try:
            return int(raw_input)
        except ValueError:
            print("{} is not a number, try again".format(raw_input))


def get_yes_or_no(question: str, default: bool) -> bool:
    options = "y/N"
    if default is True:
        options = "Y/n"
    while True:
        yes_or_no = input("{} [{}] ".format(question, options))
        if yes_or_no == "":
            return default
        if yes_or_no.lower() == "n":
            return False
        if yes_or_no.lower() == "y":
            return True


results = []
for i, question in enumerate(questions):
    results.append(False)
    print(question)
    index = get_index_input()
    if question.is_correct(index):
        print("Correct :)")
        results[i] = True
    else:
        print("!!! The correct answer is {}, with index {}".format(
            question.correct_answer(),
            question.answer_index,
        ))
    print()

print("You got {} of {} correct".format(
    sum(results),
    len(questions))
)

send_results = get_yes_or_no("Do you wanna send a results?", True)
if not send_results:
    exit(0)

ddo_email = ""
while True:
    ddo_email = input("What is your @dataddo.com name? ")
    if ddo_email == "":
        continue
    ddo_email += "@dataddo.com"
    if get_yes_or_no(ddo_email + ", is that correct?", True):
        break


def ordinal(n: int) -> str:
    i = (n // 10 % 10 != 1) * (n % 10 < 4) * n % 10
    return "{:d}{}".format(n, "tsnrhtdd"[i::4])


table = StringIO()
table.write("| Question | Correct \n")
table.write("| -------- | ------- \n")
for i, res in enumerate(results):
    print("| {:^8} | {:^7}".format(ordinal(i + 1), res), file=table)

params = {
    "view": "cm",
    "fs": "1",
    "to": "tomas.prochazka@dataddo.com",
    "su": "My quiz results",
    "body": "Hi Tomas,\nI have finished this awesome quiz and got {} of {} right.\n\n{}".format(
        sum(results), len(questions), table.getvalue()),
    "authuser": ddo_email,
}
url = "https://mail.google.com/mail/?{}".format(
    urllib.parse.urlencode(params),
)
webbrowser.open_new_tab(url)
