#!/bin/bash

if [[ "$VIRTUAL_ENV" != "" ]]
then
  jupyter nbconvert --ClearOutputPreprocessor.enabled=True --inplace *.ipynb
else
  echo "activate venv first"
fi


